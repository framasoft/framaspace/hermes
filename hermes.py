#!/usr/bin/env python3

"""Hermes, a daemon to retrieve jobs from Charon and process them

Debian dependencies:
  python3-requests
  python3-yaml
  python3-logbook

To lint the file, install python3-pycodestyle, pylint, mypy, then:
  python /usr/lib/python3/dist-packages/pycodestyle.py hermes.py
  pylint --rcfile .pylintrc hermes.py
  mypy hermes.py
"""

import argparse
import json
import signal
import shutil
import ssl
import smtplib
import sys
import time
from datetime import datetime, timedelta
from pprint import pformat
from typing import Union
from os import path, remove, uname
import requests
import logbook  # type: ignore
import yaml
from salt import client as sclient  # type: ignore
try:
    from yaml import CDumper as Dumper
except ImportError:
    from yaml import Dumper  # type: ignore

JOB_TYPE_CREATE_SPACE = 0
JOB_TYPE_UPDATE_SPACE = 10
JOB_TYPE_CHANGE_OFFICE = 15
JOB_TYPE_DISABLE_SPACE = 20
JOB_TYPE_REENABLE_SPACE = 30
JOB_TYPE_DELETE_SPACE = 50
JOB_TYPE_POST_INSTALL_ADMIN_USER_CONFIGURATION = 60
JOB_TYPE_LAST_STEP = 70
JOB_TYPE_DELETE_SHARED_OFFICE_SERVER = 80
JOB_TYPE_UNIQUE_DEPLOY_PAHEKO = 90

CHANGES = {
    JOB_TYPE_CREATE_SPACE:                          'create',
    JOB_TYPE_UPDATE_SPACE:                          'update',
    JOB_TYPE_CHANGE_OFFICE:                         'office',
    JOB_TYPE_DISABLE_SPACE:                         'disable',
    JOB_TYPE_REENABLE_SPACE:                        'reenable',
    JOB_TYPE_DELETE_SPACE:                          'delete',
    JOB_TYPE_POST_INSTALL_ADMIN_USER_CONFIGURATION: 'app-password',
    JOB_TYPE_LAST_STEP:                             'last-step',
    JOB_TYPE_DELETE_SHARED_OFFICE_SERVER:           'delete-shared-office',
    JOB_TYPE_UNIQUE_DEPLOY_PAHEKO:                  'deploy-paheko',
}
JOBS = CHANGES.values()

JOB_STATUS_PENDING = 0
JOB_STATUS_ONGOING = 10
JOB_STATUS_ERROR = 50
JOB_STATUS_DONE = 100

SPACE_STATUS_PENDING = 0
SPACE_STATUS_CREATED = 10

SPACE_OFFICE_TYPE_COLLABORA_OFFICE = 10
SPACE_OFFICE_TYPE_ONLYOFFICE = 20
SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE = 30
SPACE_OFFICE_TYPE_DEFAULT = SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE

OFFICES = {
    SPACE_OFFICE_TYPE_COLLABORA_OFFICE: 'collabora',
    SPACE_OFFICE_TYPE_ONLYOFFICE: 'onlyoffice',
    SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE: 'shared-collabora'
}

HEADERS = {'accept': 'application/json'}

INSTANCES_FILE = '/srv/pillar/framaspace/instances.sls'


log_handler = logbook.StreamHandler(
        sys.stdout,
        format_string='{record.level_name}: {record.message}',
        bubble=True)
log_handler.push_application()


def notify_charon_communication_pb(err: str):
    """Notify when we have troubles communicating with Charon

    Keyword argument:
    err -- string which should be a stacktrace
    """
    if 'gotify' in config and 'tokens' in config['gotify']:
        logger.debug('Notify of communication problem with Charon with gotify')
        payload = {'title': 'Communication problem of Hermes with Charon',
                   'message': 'Hermes couldn’t communicate with Charon.\n'
                              f'Error: {err}',
                   'priority': 9}
        notify_gotify(payload)
    if 'mail' in config and 'addresses' in config['mail']:
        logger.debug('Notify of communication problem with Charon by mail')
        msg = f"""
Subject: Communication problem of Hermes with Charon

Hermes couldn’t communicate with Charon.
Error: {err}"""
        notify_by_mail(msg)


def notify_stale_lock(locktime) -> bool:
    """Notify when we have a stale lockfile

    Keyword argument:
    locktime -- datetime of the lockfile’s modification time
    """
    if (datetime.now() - locktime) < timedelta(minutes=30):
        return False

    extra_msg = f'Please go to the server {uname().nodename} to remove the ' \
                'lock.\nYou will be notified every hour until the stale ' \
                'lock file is removed'
    priority = 9

    if 'gotify' in config and 'tokens' in config['gotify']:
        logger.debug('Notify of stale lock with gotify')
        payload = {'title': 'Stale lock file on Hermes',
                   'message': 'The lock has been created on '
                              f'{locktime.strftime("%A %d %B at %Hh%M")}.\n'
                              f'{extra_msg}',
                   'priority': priority}
        notify_gotify(payload)
    if 'mail' in config and 'addresses' in config['mail']:
        logger.debug('Notify of stale lock by mail')
        msg = f"""\
Subject: Stale lock file on Hermes

The lock has been created on {locktime.strftime('%A %d %B at %Hh%M')}.
{extra_msg}"""
        notify_by_mail(msg)
    return True


def notify_gotify(payload: dict):
    """Notify via gotify

    Keyword argument:
    payload -- dict, suitable to send to gotify
    """
    headers = {'accept': 'application/json',
               'content-type': 'application/json'}

    for token in config['gotify']['tokens']:
        logger.debug(f'Sending gotify message to {config["gotify"]["url"]}')
        logger.debug(pformat(payload, indent=2, sort_dicts=False))
        try:
            req = requests.post(f'{config["gotify"]["url"]}/message',
                                params={'token': token},
                                headers=headers,
                                timeout=60,
                                data=json.dumps(payload))
        except requests.exceptions.RequestException as req_err:
            logger.error('[GOTIFY] Requests error: '
                         f'{pformat(req_err, indent=2, sort_dicts=False)}')
        else:
            if req.status_code != 200:
                logger.error(f'HTTPError: {req.status_code} {req.reason}')


def notify_by_mail(msg: str) -> bool:
    """Notify via mail

    Keyword argument:
    msg -- string, the message to sent
    """
    if 'host' not in config['mail']:
        config['mail']['host'] = '127.0.0.1'
    if 'port' not in config['mail']:
        config['mail']['port'] = 25
    if 'ssl' not in config['mail']:
        config['mail']['ssl'] = False
    if 'starttls' not in config['mail']:
        config['mail']['starttls'] = False

    if config['mail']['ssl']:
        logger.debug('Mail notification: SSL')
        context = ssl.create_default_context()
        smtp = smtplib.SMTP_SSL(host=config['mail']['host'],
                                port=config['mail']['port'],
                                context=context)
    else:
        smtp = smtplib.SMTP(host=config['mail']['host'],  # type: ignore
                            port=config['mail']['port'])
        if config['mail']['starttls']:
            logger.debug('Mail notification: STARTTLS')
            context = ssl.create_default_context()
            smtp.starttls(context=context)

    if 'auth' in config['mail']:
        if 'login' not in config['mail']['auth'] \
           or 'password' not in config['mail']['auth']:
            logger.warning('Mail credentials are incomplete. '
                           'No mail authentication will be done')
            return False

        logger.debug('Mail notification: authentification')
        smtp.login(config['mail']['auth']['login'],
                   config['mail']['auth']['password'])

    for address in config['mail']['addresses']:
        logger.debug(f'Sending mail to {address}')
        logger.debug(msg)
        smtp.sendmail(config['mail']['from'], address, msg)

    return True


def sigterm_handler(signum: int, frame):
    """Clean exit on SIGTERM

    Keyword argument:
    signum -- int, the signal number
    frame  -- frame object, see
             https://docs.python.org/3/reference/datamodel.html#frame-objects
    """
    logger.info(f'Got {signal.Signals(signum).name} ({signum}) signal, '
                'removing lockfiles and exiting')
    logger.debug(frame)
    if not no_lockfile():
        delete_lockfile()
    for job in JOBS:
        if not no_lockfile(job):
            delete_lockfile(job)

    sys.exit(0)


def lockfile(job: Union[str, None] = None) -> str:
    """Returns the location of the lockfile, depending on the jobtype

    Keyword argument:
    job -- string, job type
    """
    if job is None:
        return '/tmp/hermes.lock'

    return f'/tmp/hermes-{job}.lock'


def no_lockfile(job: Union[str, None] = None) -> bool:
    """Say in the lockfile does not exist

    Keyword argument:
    job -- string, job type
    """
    return not path.exists(lockfile(job))


def age_of_lockfile(job: Union[str, None] = None) -> datetime:
    """Return the motification time of the lockfile

    Keyword argument:
    job -- string, job type
    """
    mtime = path.getmtime(lockfile(job))
    return datetime.fromtimestamp(mtime)


def create_lockfile(job: Union[str, None] = None):
    """Create a lockfile

    Keyword argument:
    job -- string, job type
    """
    with open(lockfile(job), mode='w', encoding='utf-8') as file_lock:
        file_lock.close()


def delete_lockfile(job: Union[str, None] = None):
    """Delete a lockfile

    Keyword argument:
    job -- string, job type
    """
    remove(lockfile(job))


def is_salt_call_success(domain: str, jobtype: str, ret: dict,
                         job_id: Union[int, None] = None,
                         operation: Union[str, None] = None) -> bool:
    """ Analyse salt payload

    Keyword argument:
    domain    -- string, the domain of the instance
    jobtype   -- string, type of the job
    ret       -- dict, full return of a salt job
    job_id    -- int, id of the job
    operation -- string, identify servers on which the job is run
    """
    count = 0
    if operation is not None:
        jobtype = f'{jobtype}][{operation}'

    for server, result in ret.items():
        count += result['retcode']
        if result['retcode'] != 0:
            err = f"""[{domain}] [{jobtype.upper()}] server {server} has a \
return code of {result["retcode"]}. Output:
  {pformat(result["ret"], indent=2, sort_dicts=False)}"""
            logger.warning(err)
            if job_id is not None:
                end_job_with_error(domain, job_id, err)
        else:
            logger.info(f'[{domain}] [{jobtype.upper()}] server {server} '
                        f'has a return code of {result["retcode"]}.')
            logger.debug(f"""Output:
  {pformat(result["ret"], indent=2, sort_dicts=False)}""")

    return count == 0


def end_job_with_error(domain: str, job_id: int, err: str) -> bool:
    """ Analyse salt payload

    Keyword argument:
    domain -- string, the domain of the instance
    job_id -- int, id of the job
    err    -- string, the error to send to Charon
    """
    if args.nocontact:
        return True

    payload = {
        'success': False,
        'payload': err
    }
    logger.error(f'[{domain}] POSTing to '
                 f'{config["host"]}/api/v1/jobs/{job_id}: setting job to '
                 'ERROR')
    logger.error(f'{err}')
    try:
        req = requests.post(f'{config["host"]}/api/v1/jobs/{job_id}',
                            auth=(config['login'], config['passwd']),
                            timeout=60,
                            json=payload)
    except requests.exceptions.RequestException as req_err:
        err_str = pformat(req_err, indent=2, sort_dicts=False)
        logger.error(f'[{domain}] Requests error: {err_str}')
        notify_charon_communication_pb(err_str)
        return False

    if req.status_code != 200:
        logger.error(f'[{domain}] HTTPError: {req.status_code} '
                     f'{req.reason}')
        return False

    return True


def end_job_with_success(domain: str, job_id: int,
                         provided_payload: Union[dict, None] = None) -> bool:
    """ Tell Charon that the job ended successfully

    Keyword argument:
    domain           -- string, the domain of the instance
    job_id           -- int, id of the job
    provided_payload -- dict, extra information to send to Charon
    """
    if args.nocontact:
        return True

    payload = {
        'success': True,
        'payload': provided_payload
    }

    logger.info(f'[{domain}] POSTing to '
                f'{config["host"]}/api/v1/jobs/{job_id}: '
                'setting job to SUCCESS')
    logger.debug(f'[{domain}] payload: '
                 f'{pformat(config, indent=2, width=1, sort_dicts=False)}')
    try:
        req = requests.post(f'{config["host"]}/api/v1/jobs/{job_id}',
                            auth=(config['login'], config['passwd']),
                            timeout=60,
                            json=payload)
    except requests.exceptions.RequestException as req_err:
        err_str = pformat(req_err, indent=2, sort_dicts=False)
        logger.error(f'[{domain}] Requests error: {err_str}')
        notify_charon_communication_pb(err_str)
        return False

    if req.status_code != 200:
        logger.error(f'[{domain}] HTTPError: {req.status_code} '
                     f'{req.reason}')
        return False

    return True


def set_job_to_ongoing(domain: str, job_id: int) -> bool:
    """ Tell Charon that we begin to process the job

    Keyword argument:
    domain -- string, the domain of the instance
    job_id -- int, id of the job
    """
    if args.nocontact:
        return True

    logger.info(f'[{domain}] PUTing to '
                f'{config["host"]}/api/v1/jobs/{job_id}: '
                'setting job to ONGOING')
    try:
        req = requests.put(f'{config["host"]}/api/v1/jobs/{job_id}',
                           auth=(config['login'], config['passwd']),
                           timeout=60,
                           headers=HEADERS)
    except requests.exceptions.RequestException as req_err:
        err_str = pformat(req_err, indent=2, sort_dicts=False)
        logger.error(f'[{domain}] Requests error: {err_str}')
        notify_charon_communication_pb(err_str)
        return False

    if req.status_code != 200:
        logger.error(f'[{domain}] HTTPError: {req.status_code} '
                     f'{req.reason}')
        return False

    return True


def check_and_filter_jobs(jobs: dict):  # pylint: disable-msg=R0912
    """ Verify that there is no strange jobs that shouldn’t happen

    Keyword argument:
    jobs -- array of jobs to clean up
    """
    global pillar  # pylint: disable-msg=C0103,W0601
    with open(INSTANCES_FILE, mode='r', encoding='utf-8') as i_file:
        pillar = yaml.safe_load(i_file)
        i_file.close()

    if pillar is None or 'framaspace-instances' not in pillar:
        pillar = {'framaspace-instances': {}}

    salt_jobs: dict = {}
    for i in JOBS:
        salt_jobs[i] = {}

    logger.info('Filtering pending jobs')
    if args.exception is not None:
        logger.info('All jobs will be processed except '
                    f'{", ".join(args.exception)}')
    elif args.only is not None:
        logger.info(f'Only {" and ".join(args.only)} job will be processed')

    for job in jobs:
        if job['type'] != JOB_TYPE_DELETE_SHARED_OFFICE_SERVER:
            domain = job['data']['domain']
            status = find_status_of_instance_in_pillar(domain, True)

        if args.exception is not None and \
           CHANGES[job['type']] in args.exception:
            continue
        if args.only is not None and CHANGES[job['type']] not in args.only:
            continue

        if job['type'] == JOB_TYPE_CREATE_SPACE:
            # ! try to create space that already exists
            if status is not None:
                logger.error(f'[{domain}] Error: Trying to create {domain} '
                             'while it is already in framaspace instances, '
                             'this should not happen!')
                end_job_with_error(domain,
                                   job['id'],
                                   f'The domain {domain} is already in '
                                   'framaspace instances, this should not '
                                   'happen')
                continue

            job['data']['id'] = job['id']
            salt_jobs['create'][domain] = job['data']
        elif job['type'] == JOB_TYPE_DELETE_SHARED_OFFICE_SERVER:
            job['data']['id'] = job['id']
            salt_jobs['delete-shared-office'][job['id']] = job['data']
        # ! try to modify/delete a domain that does not exist
        elif status is None:
            logger.error(
                    f'[{domain}] Error: Trying to update {domain} while it '
                    'is not in framaspace instances, this should not happen! '
                    f'(job type: {job["type"]})')
            end_job_with_error(
                    domain,
                    job['id'],
                    f'The domain {domain} is not in framaspace instances, '
                    'this should not happen')
            continue
        else:
            job['data']['id'] = job['id']
            salt_jobs[CHANGES[job['type']]][domain] = job['data']

    return salt_jobs


def dump_job_data(job_type: str, domain: str, data: dict):
    """ Dump job data to pillar

    Keyword argument:
    job_type -- string, the type of job
    domain   -- string, the domain of the instance
    data     -- dict, details of the job
    """
    logger.info(f'[{domain}] Dumping data on {job_type}')

    with open(f'/srv/pillar/framaspace/jobs/{job_type}.sls',
              mode='w', encoding='utf-8') as p_file:
        yaml.dump({f'framaspace-jobs-{job_type}': {domain: data}},
                  p_file, Dumper=Dumper)


def dump_instances_to_pillar():
    """ Dump updated instances’ informations to pillar
    """
    logger.info('Dump updated framaspace-instances to pillar file')

    with open(INSTANCES_FILE, mode='w', encoding='utf-8') as i_file:
        yaml.dump({'framaspace-instances':
                   pillar['framaspace-instances']},
                  i_file,
                  encoding='utf-8',
                  indent=2,
                  default_flow_style=False,
                  Dumper=Dumper)
        i_file.close()


def empty_job_pillar(job_type: str):
    """ Clean up (empty) pillar file

    Keyword argument:
    job_type -- string, the type of job
    """
    logger.info(f'Emptying framaspace-jobs-{job_type} pillar')

    with open(f'/srv/pillar/framaspace/jobs/{job_type}.sls',
              mode='w', encoding='utf-8') as j_file:
        yaml.dump({f'framaspace-jobs-{job_type}': {}}, j_file, Dumper=Dumper)


def load_job_data():
    """ Load job details from pillar

    Only used when using 'nodl' option
    """
    salt_jobs = {}
    if args.exception is not None:
        logger.info('All jobs will be processed except '
                    f'{", ".join(args.exception)}')
    elif args.only is not None:
        logger.info(f'Only {" and ".join(args.only)} job will be processed')

    for job_type in CHANGES.values():
        salt_jobs[job_type] = {}

        if args.exception is not None and job_type in args.exception:
            continue
        if args.only is not None and job_type not in args.only:
            continue

        logger.info('Loading data from '
                    f'/srv/pillar/framaspace/jobs/{job_type}.sls')

        with open(f'/srv/pillar/framaspace/jobs/{job_type}.sls',
                  mode='r', encoding='utf-8') as j_file:
            tmp = yaml.safe_load(j_file)
            j_file.close()
            salt_jobs[job_type] = tmp[f'framaspace-jobs-{job_type}']

    return salt_jobs


def create_app_password(domain: str, data: dict):
    """ Create a nextcloud app password on the instance

    Keyword argument:
    domain -- string, the domain of the instance
    data   -- dict, details of the instance
    """
    dump_job_data(CHANGES[JOB_TYPE_POST_INSTALL_ADMIN_USER_CONFIGURATION],
                  domain, data)

    logger.info(f'[{domain}] Creating app password on '
                f'{fspace_config["framaspace"]["nginx"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['nginx']['primary'], 'state.sls',
                 ['framaspace.nextcloud.jobs.app-password'],
                 full_return=True)
    if is_salt_call_success(domain, 'create', i, data['id'], 'nginx-primary'):
        logger.info(f'[{domain}] Ending app password creation without errors')

        ret = i[fspace_config['framaspace']['nginx']['primary']]['ret']
        operations = list(ret.keys())
        return ret[operations[0]]['changes']['stdout']

    return None


def delete_shared_office(job_id: int, data: dict) -> bool:
    """ Delete shared office container

    Keyword argument:
    job_id -- int, id of the job
    data   -- dict, details of the office container to delete
    """
    dump_job_data(CHANGES[JOB_TYPE_DELETE_SHARED_OFFICE_SERVER],
                  str(job_id), data)

    server = data['shared_office_server']
    port = data['shared_office_port']

    logger.info(f'[{server}-{port}] Beginning delete shared office job '
                f'{server}-{port}')
    set_job_to_ongoing(server, job_id)

    logger.info(f'[{server}-{port}] Deleting the shared office backend on '
                'haproxy servers')
    i = salt.cmd('haproxy-*', 'state.sls',
                 ['framaspace.haproxy.jobs.delete-shared-office'],
                 full_return=True)
    if not is_salt_call_success(server, 'delete-shared-office', i,
                                job_id, server):
        return False

    logger.info(f'[{server}-{port}] Deleting the shared office '
                f'{server}-{port}')
    i = salt.cmd(server, 'state.sls',
                 ['framaspace.office.jobs.delete-shared-office'],
                 full_return=True)
    if is_salt_call_success(server, 'delete-shared-office', i,
                            job_id, server):
        logger.info(f'[{server}-{port}] Ending delete shared office '
                    f'{server}-{port} without errors')
        return True

    return False


def last_step(domain: str, data: dict) -> bool:
    """ The final step for instance creation

    Keyword argument:
    domain -- string, the domain of the instance
    data   -- dict, details of the instance
    """
    dump_job_data(CHANGES[JOB_TYPE_LAST_STEP], domain, data)

    logger.info(f'[{domain}] Beginning last step job')
    set_job_to_ongoing(domain, data['id'])

    logger.info(f'[{domain}] Applying last step on '
                f'{fspace_config["framaspace"]["nginx"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['nginx']['primary'], 'state.sls',
                 ['framaspace.nextcloud.jobs.last-step'],
                 full_return=True)
    if is_salt_call_success(domain, 'last-step', i,
                            data['id'], 'nginx-primary'):
        return True

    return False


def delete_space(domain: str,  # pylint: disable-msg=too-many-return-statements
                 data: dict) -> bool:
    """ Delete an instance

    Keyword argument:
    domain -- string, the domain of the instance
    data   -- dict, details of the instance
    """
    dump_job_data(CHANGES[JOB_TYPE_DELETE_SPACE], domain, data)

    logger.info(f'[{domain}] Beginning space deleting')
    set_job_to_ongoing(domain, data['id'])

    # Chronos
    logger.info(f'[{domain}] Removing space from chronos on '
                f'{fspace_config["framaspace"]["nginx"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['nginx']['primary'],
                 'state.sls',
                 ['framaspace.nextcloud.jobs.chronos.delete'],
                 full_return=True)
    if not is_salt_call_success(domain, 'create', i,
                                data['id'], 'nginx-primary'):
        return False

    # Nginx/PHP
    logger.info(f'[{domain}] Removing space on all Nginx servers')
    i = salt.cmd('nginx-*',
                 'state.sls', ['framaspace.nextcloud.jobs.delete'],
                 full_return=True)
    if not is_salt_call_success(domain, 'delete', i,
                                data['id'], 'nginx-all'):
        return False

    logger.info(f'[{domain}] Deleting Paheko space')
    i = salt.cmd('nginx-*',
                 'state.sls', ['framaspace.paheko.jobs.delete'],
                 full_return=True)
    if not is_salt_call_success(domain, 'delete', i, data['id'], 'nginx-all'):
        return False

    # Haproxy (for office)
    logger.info(f'[{domain}] Deleting office backend in haproxy')
    i = salt.cmd('haproxy-*',
                 'state.sls', ['framaspace.haproxy.jobs.delete'],
                 full_return=True)
    if not is_salt_call_success(domain, 'delete', i,
                                data['id'], 'haproxy-all'):
        return False

    # # High performance backend
    # logger.info(f'[{domain}] Removing high performance backend')
    # i = salt.cmd('notify-push-*',
    #              'state.sls', ['framaspace.notify-push.jobs.delete'],
    #              full_return=True)
    # if not is_salt_call_success(domain, 'delete', i,
    #                             data['id'], 'notify-push-all'):
    #     return False

    # Office
    logger.info(f'[{domain}] Removing office container')
    i = salt.cmd('office-*',
                 'state.sls', ['framaspace.office.jobs.delete'],
                 full_return=True)
    if not is_salt_call_success(domain, 'delete', i,
                                data['id'], 'office-all'):
        return False

    # Minio
    logger.info(f'[{domain}] Removing bucket, user and policy on '
                f'{fspace_config["framaspace"]["minio"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['minio']['primary'],
                 'state.sls', ['framaspace.minio.jobs.delete'],
                 full_return=True)
    if not is_salt_call_success(domain, 'delete', i,
                                data['id'], 'minio-primary'):
        return False

    # Database
    logger.info(f'[{domain}] Removing database on '
                f'{fspace_config["framaspace"]["pg"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['pg']['primary'],
                 'state.sls', ['framaspace.postgresql.jobs.delete'],
                 full_return=True)
    if not is_salt_call_success(domain, 'delete', i,
                                data['id'], 'postgresql-primary'):
        return False

    logger.info(f'[{domain}] Ending space deleting without errors')
    return True


def disable_space(domain: str, data: dict) -> bool:
    """ Disable an instance

    Keyword argument:
    domain -- string, the domain of the instance
    data   -- dict, details of the instance
    """
    dump_job_data(CHANGES[JOB_TYPE_DISABLE_SPACE], domain, data)

    logger.info(f'[{domain}] Beginning space disabling')
    set_job_to_ongoing(domain, data['id'])

    # Chronos
    logger.info(f'[{domain}] Removing space from chronos on '
                f'{fspace_config["framaspace"]["nginx"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['nginx']['primary'],
                 'state.sls',
                 ['framaspace.nextcloud.jobs.chronos.disable'],
                 full_return=True)
    if not is_salt_call_success(domain, 'create', i,
                                data['id'], 'nginx-primary'):
        return False

    # Nginx/PHP
    logger.info(f'[{domain}] Disabling space on all Nginx servers')
    i = salt.cmd('nginx-*',
                 'state.sls', ['framaspace.nextcloud.jobs.disable'],
                 full_return=True)
    if not is_salt_call_success(domain, 'disable', i, data['id'], 'nginx-all'):
        return False

    logger.info(f'[{domain}] Disabling Paheko space')
    i = salt.cmd('nginx-*',
                 'state.sls', ['framaspace.paheko.jobs.disable'],
                 full_return=True)
    if not is_salt_call_success(domain, 'disable', i, data['id'], 'nginx-all'):
        return False

    # Haproxy (for office)
    logger.info(f'[{domain}] Disabling office backend in haproxy')
    i = salt.cmd('haproxy-*',
                 'state.sls', ['framaspace.haproxy.jobs.disable'],
                 full_return=True)
    if not is_salt_call_success(domain, 'disable', i,
                                data['id'], 'haproxy-all'):
        return False

    # # High performance backend
    # logger.info(f'[{domain}] Disabling high performance backend')
    # i = salt.cmd('notify-push-*',
    #              'state.sls', ['framaspace.notify-push.jobs.disable'],
    #              full_return=True)
    # if not is_salt_call_success(domain, 'disable', i,
    #                             data['id'], 'notify-push-all'):
    #     return False

    # Office
    logger.info(f'[{domain}] Disabling office container')
    i = salt.cmd('office-*',
                 'state.sls', ['framaspace.office.jobs.disable'],
                 full_return=True)
    if not is_salt_call_success(domain, 'disable', i,
                                data['id'], 'office-all'):
        return False

    logger.info(f'[{domain}] Ending space disabling without errors')
    return True


def create_space(domain: str, data: dict) -> bool:  # pylint: disable-msg=R0911
    """ Create an instance

    Keyword argument:
    domain -- string, the domain of the instance
    data   -- dict, details of the instance
    """
    dump_job_data(CHANGES[JOB_TYPE_CREATE_SPACE], domain, data)

    logger.info(f'[{domain}] Beginning space creation')
    set_job_to_ongoing(domain, data['id'])

    # Office
    logger.info(f'[{domain}] Creating office container')
    i = salt.cmd('office-*',
                 'state.sls', ['framaspace.office.jobs.create'],
                 full_return=True)
    if not is_salt_call_success(domain, 'create', i, data['id'], 'office-all'):
        return False

    # # High performance backend
    # logger.info(f'[{domain}] Creating high performance backend')
    # i = salt.cmd('notify-push-*',
    #              'state.sls', ['framaspace.notify-push.jobs.create'],
    #              full_return=True)
    # if not is_salt_call_success(domain, 'create', i, data['id'],
    #                             'notify-push-all'):
    #     return False

    # Haproxy (for office and high performance backend)
    logger.info(f'[{domain}] Creating office backend in haproxy')
    i = salt.cmd('haproxy-*',
                 'state.sls', ['framaspace.haproxy.jobs.create'],
                 full_return=True)
    if not is_salt_call_success(domain, 'create', i,
                                data['id'], 'haproxy-all'):
        return False

    # Database
    logger.info(f'[{domain}] Creating database on '
                f'{fspace_config["framaspace"]["pg"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['pg']['primary'],
                 'state.sls', ['framaspace.postgresql.jobs.create'],
                 full_return=True)
    if not is_salt_call_success(domain, 'create', i,
                                data['id'], 'postgresql-primary'):
        return False

    # Minio
    logger.info(f'[{domain}] Creating bucket, user and policy on '
                f'{fspace_config["framaspace"]["minio"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['minio']['primary'],
                 'state.sls', ['framaspace.minio.jobs.create'],
                 full_return=True)
    if not is_salt_call_success(domain, 'create', i,
                                data['id'], 'minio-primary'):
        return False

    time.sleep(1)

    # Nginx/PHP
    logger.info(f'[{domain}] Refreshing Demeter on all Nginx servers')
    i = salt.cmd('nginx-*',
                 'state.sls', ['framaspace.nextcloud.demeter'],
                 full_return=True)
    if not is_salt_call_success(domain, 'create', i, data['id'], 'nginx-all'):
        return False

    logger.info(f'[{domain}] Creating Paheko space')
    i = salt.cmd('nginx-*',
                 'state.sls', ['framaspace.paheko.jobs.create'],
                 full_return=True)
    if not is_salt_call_success(domain, 'create', i, data['id'], 'nginx-all'):
        return False

    logger.info(f'[{domain}] Creating space on '
                f'{fspace_config["framaspace"]["nginx"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['nginx']['primary'],
                 'state.sls', ['framaspace.nextcloud.jobs.create'],
                 full_return=True)
    if not is_salt_call_success(domain, 'create', i,
                                data['id'], 'nginx-primary'):
        return False

    logger.info(f'[{domain}] Creating space on all Nginx servers')
    i = salt.cmd('nginx-*',
                 'state.sls', ['framaspace.nextcloud.jobs.create'],
                 full_return=True)
    if not is_salt_call_success(domain, 'create', i, data['id'], 'nginx-all'):
        return False

    # Chronos
    logger.info(f'[{domain}] Adding space to chronos on '
                f'{fspace_config["framaspace"]["nginx"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['nginx']['primary'],
                 'state.sls', ['framaspace.nextcloud.jobs.chronos.create'],
                 full_return=True)
    if not is_salt_call_success(domain, 'create', i,
                                data['id'], 'nginx-primary'):
        return False

    # Needed to let NC populating the cache with the assets and other stuff
    logger.info(f'[{domain}] Fetching https://{domain}.'
                f'{fspace_config["framaspace"]["domain"]}/login to let NC '
                'do its assets caching')
    try:
        requests.get(f'https://{domain}.'
                     f'{fspace_config["framaspace"]["domain"]}/login',
                     timeout=60)
    except requests.exceptions.RequestException as req_err:
        err_str = pformat(req_err, indent=2, sort_dicts=False)
        logger.error(f'[{domain}] Requests error: {err_str}')
        notify_charon_communication_pb(err_str)
        logger.info(f'[{domain}] Ending space creation with a requests error')
    else:
        logger.info(f'[{domain}] Ending space creation without errors')

    return True


def reenable_space(domain: str, data: dict) -> bool:
    """ Re-enable an instance

    Keyword argument:
    domain -- string, the domain of the instance
    data   -- dict, details of the instance
    """
    dump_job_data(CHANGES[JOB_TYPE_REENABLE_SPACE], domain, data)

    logger.info(f'[{domain}] Beginning space enabling')
    set_job_to_ongoing(domain, data['id'])

    # Nginx/PHP
    logger.info(f'[{domain}] Enabling Paheko space')
    i = salt.cmd('nginx-*',
                 'state.sls', ['framaspace.paheko.jobs.reenable'],
                 full_return=True)
    if not is_salt_call_success(domain, 'reenable', i,
                                data['id'], 'nginx-all'):
        return False

    logger.info(f'[{domain}] Activating space on all Nginx servers')
    i = salt.cmd('nginx-*',
                 'state.sls', ['framaspace.nextcloud.jobs.reenable'],
                 full_return=True)
    if not is_salt_call_success(domain, 'reenable', i,
                                data['id'], 'nginx-all'):
        return False

    # Chronos
    logger.info(f'[{domain}] Adding space to chronos on '
                f'{fspace_config["framaspace"]["nginx"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['nginx']['primary'],
                 'state.sls', ['framaspace.nextcloud.jobs.chronos.reenable'],
                 full_return=True)
    if not is_salt_call_success(domain, 'create', i,
                                data['id'], 'nginx-primary'):
        return False

    # Office
    logger.info(f'[{domain}] Enabling office container')
    i = salt.cmd('office-*',
                 'state.sls', ['framaspace.office.jobs.reenable'],
                 full_return=True)
    if not is_salt_call_success(domain, 'reenable', i,
                                data['id'], 'office-all'):
        return False

    # # High performance backend
    # logger.info(f'[{domain}] Enabling high performance backend')
    # i = salt.cmd('notify-push-*',
    #              'state.sls', ['framaspace.notify-push.jobs.reenable'],
    #              full_return=True)
    # if not is_salt_call_success(domain, 'reenable', i,
    #                             data['id'], 'notify-push-all'):
    #     return False

    # Haproxy (for office)
    logger.info(f'[{domain}] Enabling office backend in haproxy')
    i = salt.cmd('haproxy-*',
                 'state.sls', ['framaspace.haproxy.jobs.reenable'],
                 full_return=True)
    if not is_salt_call_success(domain, 'reenable', i,
                                data['id'], 'haproxy-all'):
        return False

    logger.info(f'[{domain}] Ending space enabling without errors')
    return True


def change_office(domain: str, data: dict) -> bool:
    """ Change office backend for an instance

    Keyword argument:
    domain -- string, the domain of the instance
    data   -- dict, details of the instance
    """
    dump_job_data(CHANGES[JOB_TYPE_CHANGE_OFFICE], domain, data)

    logger.info(f'[{domain}] Beginning changing office from '
                f'{OFFICES[data["old_office_type"]]} on '
                f'{data["old_office_server"]} to '
                f'{OFFICES[data["office_type"]]} on '
                f'{data["office_server"]}')
    set_job_to_ongoing(domain, data['id'])

    if SPACE_OFFICE_TYPE_SHARED_COLLABORA_OFFICE in [data['office_type'],
                                                     data['old_office_type']]:
        logger.info(f'[{domain}] Modifying shared collabora on haproxy')
        i = salt.cmd('haproxy-*',
                     'state.sls', ['framaspace.haproxy.jobs.office'],
                     full_return=True)
        if not is_salt_call_success(domain, 'office', i,
                                    data['id'], 'haproxy-all'):
            return False

    # Office
    logger.info(f'[{domain}] Changing office container')
    i = salt.cmd('office-*',
                 'state.sls', ['framaspace.office.jobs.office'],
                 full_return=True)
    if not is_salt_call_success(domain, 'office', i, data['id'], 'office-all'):
        return False

    logger.info(f'[{domain}] Sleeping 15 seconds')
    time.sleep(15)

    # Nginx/PHP
    logger.info(f'[{domain}] Modifying office type on '
                f'{fspace_config["framaspace"]["nginx"]["primary"]} '
                'and refreshing '
                '/opt/check-onlyoffice-connection-instances.txt '
                'on all Nginx servers')
    i = salt.cmd('nginx-*',
                 'state.sls', ['framaspace.nextcloud.jobs.office'],
                 full_return=True)
    if not is_salt_call_success(domain, 'office', i,
                                data['id'], 'nginx-all'):
        return False

    logger.info(f'[{domain}] Ending changing office without errors')
    return True


def deploy_paheko(domain: str, data: dict) -> bool:
    """ Deploy Paheko for an existing instance

    Keyword argument:
    domain -- string, the domain of the instance
    data   -- dict, details of the instance
    """
    dump_job_data(CHANGES[JOB_TYPE_UNIQUE_DEPLOY_PAHEKO], domain, data)

    logger.info(f'[{domain}] Beginning deployement of Paheko on '
                f'{data["paheko_shared_server"]}')
    set_job_to_ongoing(domain, data['id'])

    # MinIO
    logger.info(f'[{domain}] Creating bucket, user and policy on '
                f'{fspace_config["framaspace"]["minio"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['minio']['primary'],
                 'state.sls', ['framaspace.minio.jobs.deploy-paheko'],
                 full_return=True)
    if not is_salt_call_success(domain, 'paheko', i,
                                data['id'], 'minio-primary'):
        return False

    # HAProxy
    logger.info(f'[{domain}] Adding Paheko backend on haproxy')
    i = salt.cmd('haproxy-*',
                 'state.sls', ['framaspace.haproxy.jobs.deploy-paheko'],
                 full_return=True)
    if not is_salt_call_success(domain, 'paheko', i,
                                data['id'], 'haproxy-all'):
        return False

    # Paheko
    logger.info(f'[{domain}] Creating Paheko instance on '
                f'{data["paheko_shared_server"]}')
    i = salt.cmd(data['paheko_shared_server'],
                 'state.sls', ['framaspace.paheko.jobs.deploy-paheko'],
                 full_return=True)
    if not is_salt_call_success(domain, 'paheko', i,
                                data['id'], 'nginx-all'):
        return False

    # Nextcloud
    logger.info(f'[{domain}] Configuring Nextcloud instance on '
                f'{fspace_config["framaspace"]["nginx"]["primary"]}')
    i = salt.cmd(fspace_config['framaspace']['nginx']['primary'],
                 'state.sls', ['framaspace.nextcloud.jobs.deploy-paheko'],
                 full_return=True)
    if not is_salt_call_success(domain, 'paheko', i,
                                data['id'], 'nginx-primary'):
        return False

    logger.info(f'[{domain}] Ending Paheko deployement without errors')
    return True


def find_status_of_instance_in_pillar(domain: str, quiet: bool = False):
    """ Find in pillar if an instance is active or inactive

    Keyword argument:
    domain -- string, the domain of the instance
    quiet  -- bool, if False, trigger a warning if the instance can’t be found
    """
    if pillar['framaspace-instances'] is not None:
        if 'active' in pillar['framaspace-instances'] and \
                domain in pillar['framaspace-instances']['active']:
            return 'active'
        if 'inactive' in pillar['framaspace-instances'] and \
                domain in pillar['framaspace-instances']['inactive']:
            return 'inactive'
        if quiet is False:
            logger.warning(f'[{domain}] Trying to find status of {domain} in '
                           'pillar but domain is not in pillar.')
    elif quiet is False:
        logger.warning(f'[{domain}] Trying to find status of {domain} in '
                       'pillar but pillar is empty.')
    return None


def add_instance_to_pillar(category: str, domain: str, data: dict):
    """ Add instance and its details to instances pillar

    Keyword argument:
    category -- string, 'active' or 'inactive'
    domain   -- string, the domain of the instance
    data     -- dict, details of the instance
    """
    logger.info(f'[{domain}] Setting to {category} in framaspace-instances '
                'pillar')

    if pillar['framaspace-instances'] is not None:
        if category in pillar['framaspace-instances']:
            pillar['framaspace-instances'][category][domain] = data
        else:
            pillar['framaspace-instances'][category] = {domain: data}
    else:
        pillar['framaspace-instances'] = {category: {domain: data}}


def remove_instance_from_pillar(domain: str):
    """ Remove instance and its details from instances pillar

    Keyword argument:
    domain -- string, the domain of the instance
    """
    logger.info(f'[{domain}] Removing from framaspace-instances pillar')

    status = find_status_of_instance_in_pillar(domain)
    if status is not None:
        del pillar['framaspace-instances'][status][domain]
    else:
        logger.warning(f'[{domain}] Trying to remove from pillar but domain '
                       'is not in pillar.')


def update_instance_in_pillar(domain: str, data: dict):
    """ Update instance and its details in instances pillar

    Keyword argument:
    domain -- string, the domain of the instance
    data   -- dict, details of the instance
    """
    logger.info(f'[{domain}] Updating informations in '
                'framaspace-instances pillar')

    status = find_status_of_instance_in_pillar(domain)
    if status is not None:
        pillar['framaspace-instances'][status][domain] = data
    else:
        logger.warning(f'[{domain}] Trying to update in pillar but domain is '
                       'not in pillar.')


def switch_instance_status_to_in_pillar(category: str, domain: str,
                                        data: dict):
    """ Change status (active/inactive) of instance in instances pillar

    Keyword argument:
    category -- string, 'active' or 'inactive'
    domain   -- string, the domain of the instance
    data     -- dict, details of the instance
    """
    logger.info(f'[{domain}] Switching status in framaspace-instances pillar')

    status = find_status_of_instance_in_pillar(domain)
    if status is not None:
        remove_instance_from_pillar(domain)
        if status != category:
            add_instance_to_pillar(category, domain, data)
        else:
            logger.warning(f'[{domain}] Trying to switch status to '
                           f'{category} in pillar but domain is '
                           f'already in {category}.')
    else:
        logger.warning(f'[{domain}] Trying to switch status in pillar but '
                       'domain is not in pillar.')
        add_instance_to_pillar(category, domain, data)


def fetch_created_spaces(page: int) -> Union[requests.Response, None]:
    """ Ask Charon for the list of instances with status 'created'

    Keyword argument:
    page -- int, page of result
    """
    logger.info(f'GETting {config["host"]}/api/v1/spaces, page {page}')
    try:
        req = requests.get(f'{config["host"]}/api/v1/spaces',
                           auth=(config['login'], config['passwd']),
                           headers=HEADERS,
                           timeout=60,
                           params={
                               'status': SPACE_STATUS_CREATED,
                               'page': page,
                               'perPage': 50
                           })
    except requests.exceptions.RequestException as req_err:
        err_str = pformat(req_err, indent=2, sort_dicts=False)
        logger.error(f'Requests error: {err_str}')
        notify_charon_communication_pb(err_str)
        return None

    return req


def retrieve_spaces() -> int:
    """ Fetch created spaces from Charon and dump them to instances pillar
    """
    # Fetch first page
    req = fetch_created_spaces(1)

    if req is None:
        return 3

    if req.status_code != 200:
        logger.critical(f'HTTPError: {req.status_code} {req.reason}')
        return 2

    result = req.json()

    spaces = result['items']

    # Fetch other pages
    while result['pagination']['has_next_page']:
        page = result['pagination']['current_page'] + 1
        req = fetch_created_spaces(page)

        if req is None:
            return 3

        if req.status_code != 200:
            logger.critical(f'HTTPError: {req.status_code} {req.reason}')
            return 2

        result = req.json()
        spaces.extend(result['items'])

    # Too bad, nothing to do
    if len(spaces) == 0:
        logger.warning('No created spaces to use.')
        return 0

    logger.info(f'{len(spaces)} spaces fetched')

    instances: dict[str, dict] = {'active': {}, 'inactive': {}}
    for space in spaces:
        if space['domain'] in instances['active']:
            logger.error(f'{space["domain"]} is already in active instances!')
        if space['domain'] in instances['inactive']:
            logger.error(f'{space["domain"]} '
                         'is already in inactive instances!')

        # Change structure for salt use
        space['details']['domain'] = space['domain']
        space['details']['office_type'] = space['office']
        space['details']['organisation_name'] = space['organizationName']
        if space['disabledReason'] is not None:
            space['details']['disabling_reason'] = space['disabledReason']
        if space['enabled'] is True:
            instances['active'][space['domain']] = space['details']
        else:
            instances['inactive'][space['domain']] = space['details']

    logger.info(f'{len(instances["active"])} active spaces')
    logger.info(f'{len(instances["inactive"])} inactive spaces')

    # Create a copy of the salt pillar file before dumping instances into it
    logger.info(f'Copying {INSTANCES_FILE} to /tmp/')
    shutil.copy2(INSTANCES_FILE, '/tmp/')

    # Dump instances in salt pillar file
    logger.info(f'Dumping instances info in {INSTANCES_FILE}')
    with open(INSTANCES_FILE, mode='w', encoding='utf-8') as i_file:
        yaml.dump({'framaspace-instances': instances},
                  i_file,
                  encoding='utf-8',
                  indent=2,
                  default_flow_style=False,
                  Dumper=Dumper)
        i_file.close()

    return 0


def fetch_pending_jobs(page: int) -> Union[requests.Response, None]:
    """ Ask Charon for pending jobs

    Keyword argument:
    page -- int, page of result
    """
    logger.info(f'GETting {config["host"]}/api/v1/jobs, page {page}')
    try:
        req = requests.get(f'{config["host"]}/api/v1/jobs',
                           auth=(config['login'], config['passwd']),
                           headers=HEADERS,
                           timeout=60,
                           params={
                               'status': JOB_STATUS_PENDING,
                               'page': page,
                               'perPage': 50
                           })
    except requests.exceptions.RequestException as req_err:
        err_str = pformat(req_err, indent=2, sort_dicts=False)
        logger.error(f'Requests error: {err_str}')
        notify_charon_communication_pb(err_str)
        delete_lockfile()
        return None

    return req


def retrieve_jobs() -> int:  # pylint: disable-msg=R0911,R0912,R0914,R0915
    """ Fetch and process pending jobs
    """
    # Global lockfile
    if not no_lockfile():
        lockfile_age = age_of_lockfile()
        logger.warning(f'[ALL JOBS] Lockfile {lockfile()} exists. '
                       f'It has been created at {lockfile_age}')
        notify_stale_lock(lockfile_age)

        return 1

    create_lockfile()

    if args.nodl:
        global pillar  # pylint: disable-msg=C0103,W0601
        with open(INSTANCES_FILE, mode='r', encoding='utf-8') as i_file:
            pillar = yaml.safe_load(i_file)
            i_file.close()

        salt_jobs = load_job_data()
        if args.exception is not None:
            for jtype in JOBS:
                if jtype in args.exception and jtype in salt_jobs:
                    salt_jobs[jtype] = {}
        elif args.only is not None:
            for jtype in JOBS:
                if jtype not in args.only and jtype in salt_jobs:
                    salt_jobs[jtype] = {}
    else:
        # Fetch first page
        req = fetch_pending_jobs(1)

        if req is None:
            return 3

        if req.status_code != 200:
            logger.critical(f'HTTPError: {req.status_code} {req.reason}')
            delete_lockfile()
            return 2

        result = req.json()

        jobs = result['items']

        # Fetch all other pages
        while result['pagination']['has_next_page']:
            page = result['pagination']['current_page'] + 1
            req = fetch_pending_jobs(page)

            if req is None:
                return 3

            if req.status_code != 200:
                logger.critical(f'HTTPError: {req.status_code} {req.reason}')
                delete_lockfile()
                return 2

            result = req.json()
            jobs.append(result['items'])

        if len(jobs) == 0:
            logger.info('No jobs to process')
            delete_lockfile()
            return 0

        salt_jobs = check_and_filter_jobs(jobs)

    jobs_nb = 0
    for i in JOBS:
        jobs_nb += len(salt_jobs[i])
    if jobs_nb == 0:
        logger.info('No jobs to process')
        delete_lockfile()
        return 1

    global salt  # pylint: disable-msg=C0103,W0601
    salt = sclient.LocalClient()

    global fspace_config  # pylint: disable-msg=C0103,W0601
    with open('/srv/pillar/framaspace/config.sls',
              mode='r', encoding='utf-8') as c_file:
        fspace_config = yaml.safe_load(c_file)
        c_file.close()

    ##############################################################
    # Deletion and deactivation before creation and reactivation #
    # in order to release ressources before using new ones       #
    ##############################################################

    jobtype = CHANGES[JOB_TYPE_LAST_STEP]
    if len(salt_jobs[jobtype]) > 0:
        if no_lockfile(jobtype):
            create_lockfile(jobtype)
            for domain, data in salt_jobs[jobtype].items():
                if last_step(domain, data):
                    end_job_with_success(domain, data['id'])
            # Remove jobs from pillar
            empty_job_pillar(jobtype)
            delete_lockfile(jobtype)
        else:
            lockfile_age = age_of_lockfile(jobtype)
            logger.warning(f'[{jobtype.upper()}] Lockfile {lockfile(jobtype)} '
                           f'exists. It has been created at {lockfile_age}')
            notify_stale_lock(lockfile_age)

    jobtype = CHANGES[JOB_TYPE_POST_INSTALL_ADMIN_USER_CONFIGURATION]
    if len(salt_jobs[jobtype]) > 0:
        if no_lockfile(jobtype):
            create_lockfile(jobtype)
            for domain, data in salt_jobs[jobtype].items():
                app_password = create_app_password(domain, data)
                if app_password is not None:
                    end_job_with_success(domain,
                                         data['id'],
                                         {'app_password': app_password})
            # Remove jobs from pillar
            empty_job_pillar(jobtype)
            delete_lockfile(jobtype)
        else:
            lockfile_age = age_of_lockfile(jobtype)
            logger.warning(f'[{jobtype.upper()}] Lockfile {lockfile(jobtype)} '
                           f'exists. It has been created at {lockfile_age}')
            notify_stale_lock(lockfile_age)

    jobtype = CHANGES[JOB_TYPE_DELETE_SPACE]
    if len(salt_jobs[jobtype]) > 0:
        if no_lockfile(jobtype):
            create_lockfile(jobtype)
            for domain, data in salt_jobs[jobtype].items():
                if delete_space(domain, data):
                    job_id = data.pop('id')
                    remove_instance_from_pillar(domain)
                    end_job_with_success(domain, job_id)
            # Remove jobs from pillar
            empty_job_pillar(jobtype)
            delete_lockfile(jobtype)
        else:
            lockfile_age = age_of_lockfile(jobtype)
            logger.warning(f'[{jobtype.upper()}] Lockfile {lockfile(jobtype)} '
                           f'exists. It has been created at {lockfile_age}')
            notify_stale_lock(lockfile_age)

    jobtype = CHANGES[JOB_TYPE_DISABLE_SPACE]
    if len(salt_jobs[jobtype]) > 0:
        if no_lockfile(jobtype):
            create_lockfile(jobtype)
            for domain, data in salt_jobs[jobtype].items():
                if disable_space(domain, data):
                    job_id = data.pop('id')
                    switch_instance_status_to_in_pillar('inactive',
                                                        domain,
                                                        data)
                    end_job_with_success(domain, job_id)
            # Remove jobs from pillar
            empty_job_pillar(jobtype)
            delete_lockfile(jobtype)
        else:
            lockfile_age = age_of_lockfile(jobtype)
            logger.warning(f'[{jobtype.upper()}] Lockfile {lockfile(jobtype)} '
                           f'exists. It has been created at {lockfile_age}')
            notify_stale_lock(lockfile_age)

    jobtype = CHANGES[JOB_TYPE_CREATE_SPACE]
    if len(salt_jobs[jobtype]) > 0:
        if no_lockfile(jobtype):
            create_lockfile(jobtype)
            for domain, data in salt_jobs[jobtype].items():
                if create_space(domain, data):
                    job_id = data.pop('id')
                    add_instance_to_pillar('active', domain, data)
                    end_job_with_success(domain, job_id)
                    time.sleep(1)
            # Remove jobs from pillar
            empty_job_pillar(jobtype)
            delete_lockfile(jobtype)
        else:
            lockfile_age = age_of_lockfile(jobtype)
            logger.warning(f'[{jobtype.upper()}] Lockfile {lockfile(jobtype)} '
                           f'exists. It has been created at {lockfile_age}')
            notify_stale_lock(lockfile_age)

    jobtype = CHANGES[JOB_TYPE_REENABLE_SPACE]
    if len(salt_jobs[jobtype]) > 0:
        if no_lockfile(jobtype):
            create_lockfile(jobtype)
            for domain, data in salt_jobs[jobtype].items():
                if reenable_space(domain, data):
                    job_id = data.pop('id')
                    switch_instance_status_to_in_pillar('active', domain, data)
                    end_job_with_success(domain, job_id)
            # Remove jobs from pillar
            empty_job_pillar(jobtype)
            delete_lockfile(jobtype)
        else:
            lockfile_age = age_of_lockfile(jobtype)
            logger.warning(f'[{jobtype.upper()}] Lockfile {lockfile(jobtype)} '
                           f'exists. It has been created at {lockfile_age}')
            notify_stale_lock(lockfile_age)

    jobtype = CHANGES[JOB_TYPE_CHANGE_OFFICE]
    if len(salt_jobs[jobtype]) > 0:
        if no_lockfile(jobtype):
            create_lockfile(jobtype)
            for domain, data in salt_jobs[jobtype].items():
                if change_office(domain, data):
                    job_id = data.pop('id')
                    update_instance_in_pillar(domain, data)
                    end_job_with_success(domain, job_id)
            # Remove jobs from pillar
            empty_job_pillar(jobtype)
            delete_lockfile(jobtype)
        else:
            lockfile_age = age_of_lockfile(jobtype)
            logger.warning(f'[{jobtype.upper()}] Lockfile {lockfile(jobtype)} '
                           f'exists. It has been created at {lockfile_age}')
            notify_stale_lock(lockfile_age)

    jobtype = CHANGES[JOB_TYPE_DELETE_SHARED_OFFICE_SERVER]
    if len(salt_jobs[jobtype]) > 0:
        if no_lockfile(jobtype):
            create_lockfile(jobtype)
            for job_id, data in salt_jobs[jobtype].items():
                if delete_shared_office(job_id, data):
                    end_job_with_success(f'{data["shared_office_server"]}-'
                                         f'{data["shared_office_port"]}',
                                         job_id)
            # Remove jobs from pillar
            empty_job_pillar(jobtype)
            delete_lockfile(jobtype)
        else:
            lockfile_age = age_of_lockfile(jobtype)
            logger.warning(f'[{jobtype.upper()}] Lockfile {lockfile(jobtype)} '
                           f'exists. It has been created at {lockfile_age}')
            notify_stale_lock(lockfile_age)

    jobtype = CHANGES[JOB_TYPE_UNIQUE_DEPLOY_PAHEKO]
    if len(salt_jobs[jobtype]) > 0:
        if no_lockfile(jobtype):
            create_lockfile(jobtype)
            for domain, data in salt_jobs[jobtype].items():
                if deploy_paheko(domain, data):
                    job_id = data.pop('id')
                    update_instance_in_pillar(domain, data)
                    end_job_with_success(domain, job_id)
            # Remove jobs from pillar
            empty_job_pillar(jobtype)
            delete_lockfile(jobtype)
        else:
            lockfile_age = age_of_lockfile(jobtype)
            logger.warning(f'[{jobtype.upper()}] Lockfile {lockfile(jobtype)} '
                           f'exists. It has been created at {lockfile_age}')
            notify_stale_lock(lockfile_age)

    # Dump instances in salt pillar file
    dump_instances_to_pillar()

    # Run configured post-job
    if 'post-jobs' in config and \
       'host' in config['post-jobs'] and \
       'cmd' in config['post-jobs']:
        logger.info('Executing post-jobs salt command')
        post_job = salt.cmd(config['post-jobs']['host'], 'state.sls',
                            [config['post-jobs']['cmd']],
                            full_return=True)
        is_salt_call_success('no-domain', 'post-jobs command', post_job,
                             operation=config['post-jobs']['host'])

    # Release lock
    delete_lockfile()

    logger.info('Jobs fetched and processed')
    return 0


def main() -> int:
    """ Choose what to do
    """
    if args.jobs:
        if args.loop is not None:
            while True:
                logger.info('Fetching and processing jobs')
                retrieve_jobs()
                time.sleep(args.loop * 60)
        else:
            logger.info('Fetching and processing jobs')
            return retrieve_jobs()
    elif args.spaces:
        if args.loop is not None:
            while True:
                logger.info('Fetching spaces')
                retrieve_spaces()
                time.sleep(args.loop * 60)
        else:
            logger.info('Fetching spaces')
            return retrieve_spaces()
    else:
        logger.critical('No action asked!')
        print('')
        parser.print_help()
        return 0


if __name__ == '__main__':
    signal.signal(signal.SIGTERM, sigterm_handler)

    # Handle options
    parser = argparse.ArgumentParser()
    parser.add_argument('-j', '--jobs',
                        dest='jobs',
                        help='fetch jobs from Charon and execute them',
                        action='store_true')
    parser.add_argument('-s', '--spaces',
                        dest='spaces',
                        help='fetch domains from Charon and populate Salt '
                             'pillar',
                        action='store_true')
    parser.add_argument('-n', '--no-download',
                        dest='nodl',
                        help='don’t download jobs from Charon, '
                             'use existing pillar (only useful with -j)',
                        action='store_true')
    parser.add_argument('-c', '--no-contact',
                        dest='nocontact',
                        help='don’t send job status to Charon, '
                             '(only useful with -j)',
                        action='store_true')
    parser.add_argument('-e', '--except',
                        dest='exception',
                        nargs='+',
                        help='do all job types except one. '
                             'Takes precedence over --only '
                             '(only useful with -j)',
                        choices=JOBS)
    parser.add_argument('-o', '--only',
                        dest='only',
                        nargs='+',
                        help='limit the actions to a certain type of job. '
                             '(only useful with -j)',
                        choices=JOBS)
    parser.add_argument('-v', '--verbose',
                        dest='verbose',
                        help='verbose output',
                        action='store_true')
    parser.add_argument('-d', '--debug',
                        dest='debug',
                        help='insanely verbose output',
                        action='store_true')
    parser.add_argument('--no-stdout',
                        dest='no_stdout',
                        help='don’t write messages on stdout, use only log '
                        'file',
                        action='store_true')
    parser.add_argument('-l', '--log',
                        dest='logfile',
                        help='choose where to log output '
                             '(default: /tmp/hermes.log)',
                        default='/tmp/hermes.log')
    parser.add_argument('--loop',
                        dest='loop',
                        type=int,
                        help='run Hermes in loop. The argument is the delay '
                             'in minutes between each loop')

    args = parser.parse_args()

    # Init logger
    logbook.set_datetime_format("local")

    if args.no_stdout:
        log_handler.pop_application()
    if args.exception is None:
        if args.only is None:
            logger = logbook.Logger('hermes', level=logbook.WARNING)
        else:
            logger = logbook.Logger(f'hermes-only-{"_".join(args.only)}',
                                    level=logbook.WARNING)
    else:
        logger = logbook.Logger(f'hermes-except-{"_".join(args.exception)}',
                                level=logbook.WARNING)

    if args.debug:
        logger.level = logbook.DEBUG
    if args.verbose:
        logger.level = logbook.INFO

    logfile_handler = logbook.FileHandler(
            args.logfile,
            bubble=True)
    logfile_handler.push_application()

    # Read config
    with open('/etc/hermes.yml', mode='r', encoding='utf-8') as file:
        config = yaml.safe_load(file)
        file.close()

    # Check config
    error = 0  # pylint: disable-msg=C0103
    if 'host' not in config:
        error += 1
    if 'login' not in config:
        error += 2
    if 'passwd' not in config:
        error += 4
    elif error > 0:
        config['passwd'] = 'REDACTED'

    if error > 0:
        logger.critical(
                'Error: Some configuration parameters are missing! Config:\n'
                f'{pformat(config, indent=2, width=1, sort_dicts=False)}')
        sys.exit(error)

    # Let’s roll!
    sys.exit(main())
