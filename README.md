# Hermes

Hermes is part of the [Framaspace ecosystem](https://framagit.org/framasoft/framaspace/).

Hermes is the service which process the jobs of [Charon](https://framagit.org/framasoft/framaspace/charon) with the help of [Salt](https://saltproject.io/).

## Dependencies

On Debian:
```bash
apt install python3-logbook python3-requests python3-yaml
```

## Installation

```bash
cd /opt
git clone https://framagit.org/framasoft/framaspace/hermes
cp hermes/hermes*.service /etc/systemd/system/
```

## Configuration

The configuration needs to be in `/etc/hermes.yml`.

For security, restrict the access to this file (replace `www-data` with the user of your web server):
```bash
chown root: /etc/hermes.yml
chmod 600 /etc/hermes.yml
```

Minimal configuration:

```yml
host: 'https://charon.example.org'
login: 'charon-user'
passwd: 's3cr3t'
```

Explanation:
- `host` is the address of your Charon instance
- `login` and `passwd` are the credentials needed by the API of your Charon instance (`HTTP_BASIC_AUTH_USERNAME` and `HTTP_BASIC_AUTH_PASSWORD_HASH` in Charon’s `.env` file. Generate the password hash with `bin/console security:hash-password`)

If you want to be notified of problems with mails, add:
```
mail:
  from: chronos@example.org
  host: 127.0.0.1
  port: 25
  ssl: False
  starttls: False
  auth:
    login: corge
    password: grault
  addresses:
    - foobar@example.org
    - bazqux@example.org
```

Only `from` and `addresses` are mandatory.
Default `host` and `port` are `127.0.0.1` and `25`, `ssl` and `starttls` are `False` and `auth` has no default value.

Please note that the mail authentification and SSL/STARTTLS connection have not been tested!
Report any issue on <https://framagit.org/framasoft/framaspace/chronos/-/issues/new>.

If you want to be notified of problems with gotify, add:
```
gotify:
  url: https://gotify.example.org
  tokens:
    - foobar
    - bazqux
```

## Start

The processus run in loop.
The number you launch the service with is the number of minutes the process will wait to ask for new jobs.

```
systemctl daemon-reload
# Handle pending jobs every 2 minutes
systemctl enable --now hermes@2.service
````

## LICENSE

© Framasoft 2022, licensed under the terms of the GNU GPLv3.
See [LICENSE](LICENSE) file
